FROM registry.gitlab.com/tokend/core/buildbase:latest

ENV RUBYOPT="-KU -E utf-8:utf-8"

WORKDIR /opt
COPY . .

RUN true \
 && apt install ruby2.5 -y \
 && gem install bundler -v 1.16.3 \
 && bundler install \
 && chmod +x entrypoint.sh \
 && mv ./entrypoint.sh /usr/local/bin/ \
 && apt update \
 && apt install golang -y

ENTRYPOINT [ "entrypoint.sh" ]
