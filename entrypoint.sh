#!/bin/bash

language=$1
revision=$2
namespace=$3

git clone https://github.com/tokend/xdr.git xdr 1>&2
(cd xdr && git checkout $revision 1>&2)

case "$language" in
"go" | "golang")
    rake -f gen.rakefile xdr:generate[xdr,go,xdr] 1>&2 || exit 1
    cd xdr
    echo "// revision: "$(git rev-parse HEAD)
    echo "// branch:   "$(git branch | grep \* | cut -d ' ' -f2)
    gofmt -w xdr_generated.go 1>&2 || exit 1
    cat xdr_generated.go
    echo "var Revision = "'"'$(git rev-parse HEAD)'"'
    ;;
"js" | "javascript")
    rake -f gen.rakefile xdr:generate[xdr,javascript,xdr] 1>&2 || exit 1
    cd xdr
    echo "// revision: "$(git rev-parse HEAD)
    echo "// branch:   "$(git branch | grep \* | cut -d ' ' -f2)
    cat xdr_generated.js
    ;;
"kotlin" | "swift" | "java" | "ruby" | "dotnet" | "openapi")
    rake -f gen.rakefile xdr:generate[generated,$language,$namespace] 1>&2 || exit 1
    ;;
"c++" | "C++" | "c" | "C")
    git clone https://gitlab.com/tokend/xdrpp.git xdrpp 1>&2
    cd xdrpp
    git checkout feature/generated_files_for_new_lib
    cd ..
    # prepare make files
    cmake -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles"  ./
    # build fake project only for generation xdr files
    cmake --build ./ --target fake --
    # create file for xdr revision
    (cd xdr && touch Revision.h && echo "#define XDR_REVISION" '"'$(git rev-parse HEAD)'"' >> Revision.h)
    # copy headers, use `generated` as volume to local machine
    cp xdr/*.h xdr/*.cpp generated/
    ;;
*)
    echo "Unsupported language"
    echo $language
    exit 1
    ;;
esac

exit 0
