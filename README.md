# xdrgen-docker

Use to generate xdr based code in your project

## Usage

    docker run registry.gitlab.com/tokend/xdrgen-docker <language> <branch_or_commit_hash> > <outputfile>

Examples:

    docker run registry.gitlab.com/tokend/xdrgen-docker go feature/delete_ledger_versions > xdr/xdr_generated.go
    docker run registry.gitlab.com/tokend/xdrgen-docker js 84135ed642bff4965ce69f9a91f566d6d525188d > src/base/generated/xdr_generated.js
    docker run --rm -v $PWD/src/xdr:/opt/generated registry.gitlab.com/tokend/xdrgen-docker c++ master